Copyright (c) 2014-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

# SBM - Single Byte Matcher

A simple simulator to demonstrate the single byte matcher engine, a matching circuit capable of matching subsets of regular expressions.
